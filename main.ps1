$data = '{
  "data": {
    "type": "post",
    "attributes": {
      "content": {
        "subject": "a",
        "body": "a"
      },
      "canvasProps": {
        "left": 100000,
        "top": 100000,
        "width": 100000
      },
      "color": "red",
      "status": "approved"
    }
  }
}'

$headers = @{
    'X-Api-Key' = 'pdltp_f58950b65155a210351b673341b260732657c978921fb701992ed274ff6c3e0f8a15ff'
    'accept' = 'application/vnd.api+json'
    'content-type' = 'application/vnd.api+json'
}

Invoke-WebRequest -Method Post -Uri 'https://api.padlet.dev/v1/boards/kn18ekozde4vbybf/posts' -Headers $headers -Body $data
